#!/bin/sh


# License: CC0 1.0 Universal
# !!! I DO NOT TAKE RESPONSIBILITY FOR ANY DAMAGE CAUSED BY THIS PROGRAM !!!


# Before: setup your disks
# Use like this: curl https://gitlab.com/xgqt/gentoo-auto/-/raw/master/gentoo-auto.sh | sh

# to install with myov: export DO_MYOV="yes"


# Post: /etc/fstab ; network


trap 'exit 128' INT
export PATH


tarball_location=$(wget -q -O - "https://gentoo.osuosl.org/releases/amd64/autobuilds/latest-stage3-amd64.txt" | grep -E '^2' | grep -o '^\S*')
tarball_name=$(basename "${tarball_location}")

echo
echo "------------------------------------------------------"
echo "[DEBUG]: tarball_location = ${tarball_location}"
echo "[DEBUG]: tarball_name     = ${tarball_name}"

wget "https://gentoo.osuosl.org/releases/amd64/autobuilds/${tarball_location}" -O "${tarball_name}"
tar xpf "${tarball_name}" -C . --xattrs-include='*.*' --numeric-owner


echo
echo "------------------------------------------------------"
echo "[DEBUG]: Mount necessary filesystems"

mount --rbind /dev ./dev
mount --make-rslave ./dev
mount -t proc /proc ./proc
mount --rbind /sys ./sys
mount --make-rslave ./sys
mount --rbind /tmp ./tmp


NPROC="$(nproc)"
EMERGE_DEFAULT_OPTS="--jobs=${NPROC} --keep-going --noreplace --oneshot --quiet"
FEATURES="parallel-install -ebuild-locks"
MAKEOPTS="--jobs=${NPROC} --load-average=${NPROC}"
USE="-perl"

cat <<EOF | chroot . /bin/bash


echo
echo "------------------------------------------------------"
echo "[DEBUG]: Source the profile"

env-update
source /etc/profile


echo
echo "------------------------------------------------------"
echo "[DEBUG]: Initial variable setup"

export EMERGE_DEFAULT_OPTS="${EMERGE_DEFAULT_OPTS}"
export FEATURES="${FEATURES}"
export MAKEOPTS="${MAKEOPTS}"
export USE="${USE}"

echo "[DEBUG]: Exported variables"
env | grep EMERGE_DEFAULT_OPTS
env | grep FEATURES
env | grep MAKEOPTS
env | grep USE


echo
echo "------------------------------------------------------"
echo "[DEBUG]: Web RSync"

echo "nameserver 1.1.1.1" >> /etc/resolv.conf

mkdir -p /var/db/repos/gentoo

emerge-webrsync
eselect news read new >/dev/null

mkdir -pv /etc/portage/repos.conf
cp -v /usr/share/portage/config/repos.conf /etc/portage/repos.conf/gentoo.conf
emerge --sync


echo
echo "------------------------------------------------------"
echo "[DEBUG]: Set profile"

eselect profile set 1


echo
echo "------------------------------------------------------"
echo "[DEBUG]: Generate locale"

cat > /etc/locale.gen <<LOCALE
C.UTF8 UTF-8
en_US ISO-8859-1
en_US.UTF-8 UTF-8
de_DE ISO-8859-1
de_DE.UTF-8 UTF-8
pl_PL.UTF-8 UTF-8
LOCALE
locale-gen
eselect locale set en_US.utf8


echo
echo "------------------------------------------------------"
echo "[DEBUG]: Set timezone"

emerge app-eselect/eselect-timezone
eselect timezone set Europe/Warsaw
emerge --config sys-libs/timezone-data

EOF


if [ -n "${DO_MYOV}" ]; then
    cat <<EOF | chroot . /bin/bash

echo
echo "------------------------------------------------------"
echo "[DEBUG]: myov installation"

emerge app-admin/stow app-eselect/eselect-repository dev-vcs/git

eselect repository add myov git https://gitlab.com/xgqt/myov
emaint sync -r myov

rm -r /etc/portage/package.accept_keywords
mkdir -pv /etc/portage/package.accept_keywords
echo "*/*::myov **" >> /etc/portage/package.accept_keywords/zz-myov


echo
echo "------------------------------------------------------"
echo "[DEBUG]: genlica installation"

emerge genlica
emerge --config genlica


EOF
fi


if [ -n "${DO_GRUB}" ]; then
    cat <<EOF | chroot . /bin/bash

echo
echo "------------------------------------------------------"
echo "[DEBUG]: Kernel and Bootloader setup"

emerge sys-boot/grub sys-kernel/gentoo-kernel-bin

EOF
fi

if [ -n "${DO_NET}" ]; then
    cat <<EOF | chroot . /bin/bash

echo
echo "------------------------------------------------------"
echo "[DEBUG]: Install OpenRC's netifrc"

emerge net-misc/netifrc

EOF
fi


echo
echo "------------------------------------------------------"
echo "[DEBUG]: Unmount filesystems"

umount -f ./dev
umount -f ./proc
umount -f ./sys
umount -f ./tmp


echo
echo "------------------------------------------------------"
echo "[DEBUG]: Finish"
